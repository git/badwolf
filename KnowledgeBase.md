<!--
SPDX-FileCopyrightText: 2019-2022 Badwolf Authors <https://hacktivis.me/projects/badwolf>
SPDX-License-Identifier: BSD-3-Clause
-->

- Similar WebKitGTK+ browser: https://www.uninformativ.de/git/lariza/
- WebKit2 Extensions Tutorial: https://blogs.igalia.com/carlosgc/2013/09/10/webkit2gtk-web-process-extensions/
- https://trac.torproject.org/projects/tor/wiki/doc/ImportantGoogleChromeBugs / https://trac.torproject.org/projects/tor/ticket/1925
- https://webkit.org/blog/3476/content-blockers-first-look/

# GObject Reference counting
- `GOBJECT_DEBUG=instance-count GTK_DEBUG=interactive %command%`
- Switch to the Statistics tab
- Press record (top-left)
- Do a bunch of actions, specially reversible ones
- Stop recording

Tiling window manager tip for WebKitGTK: Put the application and it's debugger on floating, leaving WebKitWebProcess ones in the back

# Bookmark formats
The different plain-text formats used by web browsers found in the wild, done 
in Augmented Backus–Naur Form (ABNF).

url = *CHAR
title = *CHAR

## uzbl
bookmark = URL TAB title [TAB tags]
tags = *(*CHAR SP)

Documenter comment: How does it manages when the separator (TAB here) is
present in the title?

## qutebrowser
bookmark = URL SP title
