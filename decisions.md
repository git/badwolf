<!--
SPDX-FileCopyrightText: 2019-2023 Badwolf Authors <https://hacktivis.me/projects/badwolf>
SPDX-License-Identifier: BSD-3-Clause
-->

# Decisions taken over the project lifetime
## What is this?
See <https://akazlou.com/posts-output/2015-11-09-every-project-should-have-decisions/>
Basically, the idea is to have a file in your projects repo, where you record
the decisions you make over the course of the project's lifetime.
Rewriting history will not be done but Post-Scriptum notices will.

## 2023-07-23 : Switch from Makefile to *.ninja
Problems:
- Makefile+config.mk pretty much reached it's limits with manpage translations as you can't just string substitute your way out when it's both prefix & suffix.
- meson/CMake/…: Still the same issues. With GNU autotools also becoming more and more problematic: <https://discourse.llvm.org/t/configure-script-breakage-with-the-new-werror-implicit-function-declaration/65213>, <https://lists.gnu.org/archive/html/autoconf-patches/2022-09/msg00011.html>, <https://wiki.gentoo.org/wiki/Modern_C_porting>

Solution picked: Transform Makefile into a static build.ninja file, and make configure generate a config.ninja file with the dynamic variables, rules and targets written in.

## 2021-09-07 : Move to a ./configure script
Problem: WebKitGTK gained a new ABI, changing 4.0 to 4.1 for libsoup3, which itself also meant some changes in badwolf

Usual solutions:
- meson: It's intended usage of having a separated build directory would mean loosing the ability to provide commands to update translations files, icons, …
- CMake: Same kind of problem as meson, while being significantly more complex and massive
- GNU autotools: Not the kind of thing I would want to depend on, specially given it's automagic behavior
- waf, scons, …: Just problems on themselves
- Bazel: Just no.

The one I picked: Custom POSIX Shell script. Slightly similar to what musl, ffmpeg, vis, … have been doing.

## 2020-10-22 : Mark the context_id as A-Z instead of Hex
The way I did it at first was to use the hexadecimal representation, but as it starts from 0 there could be confusions with a shortcut hint for alt-n, there could still be confusion with some kind of tab numbering but with a bit of usage it's hopefully going to be obvious.

## 2020-07-04 : Automatically switch to a user-created tab / Breaking the UI
I got pointed out that when creating a new tab the user has to needlessly switch to it while it could be done when they request it.
I also decided to still release it in a point release, so without bump of Major because while it does changes the behavior it doesn't break the associated meaning, while this would be a breaking change for an API I could take it as a minor adjustment in a user-interface.

Note: This would be an API if user-interface interactions could be scripted (like in HaikuOS) but I'm not aware of people actually doing this one with GTK programs.

## 2020-02-04 & 2020-02-09 : Use BSD extensions in the Makefile
The BSD extensions += and ?= for variable extension is used to avoid having to explicitly pass a list of variables to make, thus making build recipes simpler.
They are also compatible with GNU make so portability shouldn't have significantly dropped.
Which also meant dropping `.POSIX` into the file so `CC` wouldn't be set by default to `c99` (which isn't what I want, the codebase is in C11).

## 2020-01-30 02:59 : Make window declaration static
This allows to avoid a quite useless memory allocation that would only be freed at the end of the program.

## 2020-01-22 15:30 : Update copyright to Badwolf Authors
I don't want copyright assignment to me or any organisation, it should belong to every contributor, meaning that any copyright change will need to be accepted by them or their contribution would need to be replaced if need to be.

## 2020-01-08 08:42:00Z : Start of the decisions.md file
From memory and git log:

- Changelog is done when preparing for a release, it is copied into the git tag description. This means that there isn't a central file and isn't a changelog entry added with commits.
- OpenPGP is deprecated in favor of signify, reasons being security and portability.
- Prefer using lower and more standard/ubiquitous layers for implementations, one example is the about information with printf and Gtk Dialog rather than a custom scheme in WebKitGTK.
	- using GLib instead of POSIX was done at first but the quality/behaviour of GLib (like `g_malloc()` calling `abort()` on errors) reverted this decision.
- WebKitGTK was picked because it is the only WebKit port which is maintained enough without a bus-factor (consider giving QtWebKit a hand too?).
- The `.clang-format` file was copied from the one living in my home directory:
  - function declaration is at the start of the line to allow jumping to it with a simple editor
  - braces on their own lines to allow for comments space
  - "tabs for indentation and space for alignment" style is used for flexibility
- mdoc was picked over other formats for the manpage, as it is implemented in mandoc and GNU groff, much simpler to read/write and transform to other formats (like HTML or Markdown).
- Markdown was picked over other formats for informal documentation, it is a simple format supported by most forges, a format which is easier to write and parse could be considered
- Proprietary systems are not supported for their lack of auditability and transparency.
- C was picked because:
  1. I wanted to avoid being limited by binding completeness or model
  2. I think it is a good language which is fit for this purpose provided you are careful enough (such as using LLVM to it's full potential for checks)
  3. It is portable and doesn't introduces more dependencies than is already needed
- POSIX was picked for it's portability (only known one where it's absent is Windows but it's not going to be supported)
- Writing a portable (almost POSIX-only) Makefile with `pkg-config(1)` was picked over other build systems which tend to be over-complicated and rely on only one implementation.
- Originally the URL entry accepted only valid URLs, this was changed to a bit of heuristics for fixing it when it was entered by the user.
- No search entry is provided, this is to encourage users to avoid search engines as much as possible, support for SmartBookmarks with keywords (similar to DuckDuckGo/Searx !bangs) will be added after support for suggestions via bookmarks
- Bookmarks are first-class citizens as they are data explicitly controlled by the user
- No browsing history is provided, this is to encourage to use and improve bookmarks, it will be added later on with data retention limits, ability to wipe it and non-reachable from WebKit (and so the Wild Web).
- Javascript is turned off by default with a checkbox to enable it temporarily, Permissions Requests (microphone, geolocation, …) are all denied, something similar to uMatrix and NoScript will eventually be done to fix this
