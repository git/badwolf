// BadWolf: Minimalist and privacy-oriented WebKitGTK+ browser
// SPDX-FileCopyrightText: 2019-2022 Badwolf Authors <https://hacktivis.me/projects/badwolf>
// SPDX-License-Identifier: MPL-2.0

#include "bookmarks.h"

#include <glib.h>
#include <gtk/gtk.h>
#include <stdlib.h> /* setenv */

static void
bookmarks_completion_init_test(void)
{
	GtkTreeModel *result;

	char *filename = g_build_filename(g_get_current_dir(), "bookmarks_test.fixtures/full/", NULL);
	g_assert_true(setenv("XDG_DATA_HOME", filename, 1) == 0);

	result = bookmarks_completion_init();

	g_assert_true(result != NULL);
	g_free(filename);
	g_assert_true(unsetenv("XDG_DATA_HOME") == 0);
}

int
main(int argc, char *argv[])
{
	g_test_init(&argc, &argv, NULL);

	g_test_add_func("/bookmarks_completion_init/test", bookmarks_completion_init_test);

	return g_test_run();
}
