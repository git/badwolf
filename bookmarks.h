// SPDX-FileCopyrightText: 2019-2022 Badwolf Authors <https://hacktivis.me/projects/badwolf>
// SPDX-License-Identifier: MPL-2.0

#include <gtk/gtk.h>
GtkTreeModel *bookmarks_completion_init();
void bookmarks_completion_setup(GtkEntryCompletion *location_completion, GtkTreeModel *tree_model);
