#!/bin/sh

# SPDX-FileCopyrightText: 2019-2022 Badwolf Authors <https://hacktivis.me/projects/badwolf>
# SPDX-License-Identifier: BSD-3-Clause

if command -v git 2>/dev/null >/dev/null
then
	hash=$(git --git-dir="$(dirname $0)/.git" rev-parse --short HEAD 2>/dev/null)
	branch=$(git --git-dir="$(dirname $0)/.git" rev-parse --abbrev-ref HEAD 2>/dev/null | sed -r 's/.*[^0-9A-Za-z-]([0-9A-Za-z-]*)$/\1/g')

	if [ -n "$hash" ] || [ -n "$branch" ]
	then
		printf '+g%s.%s' "$hash" "$branch"
	fi
fi
echo
