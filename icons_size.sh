#!/bin/sh

# SPDX-FileCopyrightText: 2019-2022 Badwolf Authors <https://hacktivis.me/projects/badwolf>
# SPDX-License-Identifier: BSD-3-Clause

# Transforms an icon size from the Icon Theme Specification to rsvg-convert(1) options
#sed -r -e 's;^([^x]*)x([^@]*)$;-w \1 -h \2;' -e 's;^([^x]*)x([^@]*)@(.*)$;-w \1 -h \2 -z \3;'
sed -r 's;^([^x]*)x([^@]*)$;-w \1 -h \2;'
